# Toontown Racing Simulator
Hey there, Toons, and welcome to Toontown Racing Simulator! This particular project is
formerly ToonLand Mods Py3, but y'know what, screw it, we're making it exclusively about
racing!

# Contributing/Updating
I'm flattered y'all want to contribute or otherwise just fetch updates as they come! Knowledge on how to fetch and merge
through Git is GREATLY recommended, as you can easily miss things and not be able to play otherwise! (Also, there's a
nasty bug where you can't host the server yourself, but we are still working on trying to fix that. For now, though, you
can join the Dogemon Universe HUB server to get updates on when the server runs.)

# Contributors List
This list will be edited as people contribute.

- Dogemon (Maintainer)
- You! (playing or contributing)

# Discord Server
https://discord.gg/BgxzfZuWQs