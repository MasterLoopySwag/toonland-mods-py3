@echo off
cd ..
title ToonLand AI
mode con: cols=60 lines=20


rem Define some constants for our AI server:
set MAX_CHANNELS=999999
set STATESERVER=4002
set ASTRON_IP=127.0.0.1:7100
set EVENTLOGGER_IP=127.0.0.1:7198

rem Get the user input:
echo "First, type a District name. Self-explanatory, right?"
set /P DISTRICT_NAME="District name: " || ^
set DISTRICT_NAME=Ground Zero
echo "Type the base channel. Default is 401000000, increase 3rd number for additional Districts."
set /P BASE_CHANNEL="District base channel: " || ^
set BASE_CHANNEL=401000000

:main

"dependencies/panda/python/python" ^
	-m toontown.ai.ServiceStart ^
	--base-channel %BASE_CHANNEL% ^
	--max-channels %MAX_CHANNELS% ^
	--stateserver %STATESERVER% ^
	--astron-ip %ASTRON_IP% ^
	--eventlogger-ip %EVENTLOGGER_IP% ^
	--district-name "%DISTRICT_NAME%"
PAUSE
goto main
